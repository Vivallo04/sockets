#ifndef CLIENT_CLIENT_H
#define CLIENT_CLIENT_H

#include <sys/socket.h>
#include <sys/types.h>
#include <cstring>
#include <iostream>
#include <pthread.h>
#include <vector>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>


class Client
{
private:
    int TAG;
    sockaddr_in info;
    static void *Controller(void *object);

public:
    Client();
    void Connect();
    void SendMessage(const char *message);
};


#endif //CLIENT_CLIENT_H
