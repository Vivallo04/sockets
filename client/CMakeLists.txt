cmake_minimum_required(VERSION 3.19)
project(client)

set(CMAKE_CXX_STANDARD 14)

SET(CMAKE_CXX_FLAGS -pthread)
add_executable(client main.cpp Client.cpp Client.h)