#include "Client.h"

Client::Client()
{

}

void Client::Connect()
{
    TAG = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (TAG < 0)
    {
        std::cout << "ERROR! Unable to create client" << std::endl;
    }
    info.sin_family = AF_INET;
    info.sin_addr.s_addr= inet_addr("192.168.0.8");
    info.sin_port = ntohs(4050);
    memset(&info.sin_zero, 0, sizeof(info.sin_zero));

    if ((connect(TAG, (sockaddr *)&info, (socklen_t)sizeof(info))) < 0)
    {
        std::cout << "ERROR! Unable to connect to server" << std::endl;
        throw new std::exception;
    }
    std::cout << "Connected Successfully" << std::endl;3
    pthread_t clientThread;
    pthread_create(&clientThread, 0, Client::Controller, (void *)this);
    pthread_detach(clientThread);
}

void *Client::Controller(void *object) {
    Client *client = (Client *)object;
    while (true)
    {
        std::string message;
        char buffer[1024] = {0};

        while (true)
        {
            // clean the buffer
            memset(buffer, 0, 1024);
            int bytes = recv(client -> TAG, buffer, 1024, false);
            message.append(buffer, bytes);

            // if there's an error with the message remove the client
            if (bytes <= 0)
            {
                close(client -> TAG);
                pthread_exit(NULL);
            }
            if (bytes < 1024)
            {
                break;
            }
        }
        std::cout << message << std::endl;
    }
    close(client -> TAG);
    pthread_exit(NULL);
}

void Client::SendMessage(const char *message)
{
    send(TAG, message, strlen(message), false);
}


