#include "Client.h"

Client *client;
void *RunClient(void *);

int main() {
   client = new Client();
    // extra code here
    pthread_t ClientThread;
    pthread_create(&ClientThread, 0, RunClient,  NULL);
    pthread_detach(ClientThread);

    std::string json = "Hello from the Client";

    while (true)
    {
        std::string message;
        std::cin >> message;
        if (message == "Exit")
        {
            break;
        }
        client -> SendMessage(json.c_str());
    }

    delete client;
    return 0;
}


void *RunClient(void *)
{
    try
    {
        client -> Connect();
    } catch (std::string exception) {
        std::cout << exception << std::endl;
    }
    pthread_exit(NULL);

}