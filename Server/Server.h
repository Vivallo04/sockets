#ifndef CLIONPROJECTS_SERVER_H
#define CLIONPROJECTS_SERVER_H

#include <sys/socket.h>
#include <sys/types.h>
#include <cstring>
#include <iostream>
#include <pthread.h>
#include <vector>

// unix standard library
#include <unistd.h>

//network libraries
#include <netdb.h>

struct SocketData
{
    int TAG;
    sockaddr_in socketInfo;
};


class Server
{
private:
    int TAG{}; //Unique identifier
    sockaddr_in socketInfo{}; //ServerSocket info
    std::vector<int> clients;

    bool CreateSocket();
    bool BindToKernel();
    static void *ClientController(void *object);

public:
    bool running;

    Server();
    void Run();
    void SetMessage(const char *message);

};


#endif //CLIONPROJECTS_SERVER_H
