#include "Server.h"

Server::Server()
{

}


/**
 * Accept clients, create server, send messages
 */
void Server::Run()
{
    if (!CreateSocket())
    {
        std::cout << "ERROR! Unable to create socket" << std::endl;
        throw std::exception();
    }
    if (!BindToKernel())
    {
        std::cout << "ERROR! Unable to bind to kernel" << std::endl;
        throw std::exception();
    }


    while (running)
    {
        SocketData  clientInfo;
        socklen_t socketSize = sizeof(clientInfo.socketInfo);

        // wait until a client sends a request
        clientInfo.TAG = accept(TAG, (sockaddr *)&clientInfo.socketInfo, &socketSize);

        if (clientInfo.TAG < 0)
        {
            std::cout << "ERROR! Unable to accept client" << std::endl;
            throw std::exception();
            this -> running = false;
        } else {
            clients.push_back(clientInfo.TAG);
            std::cout << "Success! Client connected" << std::endl;

            pthread_t CThread;
            pthread_create(&CThread, 0, Server::ClientController, (void *)&clientInfo);
            pthread_detach(reinterpret_cast<pthread_t>(&CThread)); //do not save any information about the thread
        }
    }
    close(TAG);
}

bool Server::CreateSocket() {
    // Create a unique identifier
    TAG = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (TAG < 0)
        return false;

    socketInfo.sin_family = AF_INET; // Connection type - ipv4
    socketInfo.sin_addr.s_addr = INADDR_ANY; // Accept any connection from any address
    socketInfo.sin_port = htons(4050); // Convert the port int (4050) to bytes

    // clear memory (sin_zero field)
    memset(&socketInfo.sin_zero, 0, sizeof(socketInfo.sin_zero));
    return true;
}


bool Server::BindToKernel()
{
    if (bind(TAG, (sockaddr *) &socketInfo, (socklen_t)sizeof(socketInfo)) < 0)
    {
        return false;
    }

    // Look for new clients and allow a maximum of 4
    listen(TAG, 4);
    return true;
}


void Server::SetMessage(const char *message)
{
    for (int i = 0; i < clients.size(); i++)
    {
        send(clients[i], message, strlen(message), 0);
    }
}


/**
 *
 * @param object
 * @return
 */
void *Server::ClientController(void *object)
{
    SocketData *data = (SocketData *) object;
    while (true)
    {
        std::string message;
        char buffer[1024] = {0};

        while (true)
        {
            // clean the buffer
            memset(buffer, 0, 1024);
            int bytes = recv(data -> TAG, buffer, 1024, false);
            message.append(buffer, bytes);

            // if there's an error with the message remove the client
            if (bytes <= 0)
            {
                close(data -> TAG);
                pthread_exit(NULL);
            }
            if (bytes < 1024)
            {
                break;
            }
        }
        std::cout << message << std::endl;
    }
    close(data -> TAG);
    pthread_exit(NULL);
}


