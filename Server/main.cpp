#include <iostream>
#include "Server.h"

Server *server;
void *RunServer(void *);


int main()
{
    server = new Server();
    pthread_t serverThread;
    pthread_create(&serverThread, 0, RunServer, NULL);
    pthread_detach(serverThread);

    std::string json = "Hello from server";

    while (true)
    {
        std::string message;
        std::cin >> message;
        if (message == "Exit") break;

        server ->SetMessage(json.c_str());
    }

    delete server;
    return 0;
}


void *RunServer(void *)
{
    try
    {
        server -> Run();
    } catch(std::string exception) {
        std::cout << exception << std::endl;
    }
    return nullptr;
}

